use bevy::prelude::Component;
use rand::{thread_rng, Rng};

use crate::{WindowSize, BASE_SPEED};

// Component - Enemy Formation (per enemy)
#[derive(Clone, Component)]
pub struct Formation {
    pub start: (f32, f32),
    pub radius: (f32, f32),
    pub pivot: (f32, f32),
    pub speed: f32,
    pub angle: f32, // change per tick
}

// Resource - Formation Maker
#[derive(Default)]
pub struct FormationMaker {
    current_template: Option<Formation>,
    current_members: u32,
}

// Formation Factory Implemenation
impl FormationMaker {
    pub fn make(&mut self, window_size: &WindowSize, formation_members_max: u32) -> Formation {
        match (&self.current_template, self.current_members >= formation_members_max) {
            // if has current template and still within max members
            (Some(template), false) => {
                self.current_members += 1;
                return template.clone();
            }

            // if first formation or other formations are full
            (None, _) | (_, true) => {
                let mut rng = thread_rng();

                let width_span = window_size.width / 2.0 + 100.0;
                let height_span = window_size.height / 2.0 + 100.0;

                let x = if rng.gen_bool(0.5) {width_span} else {-width_span};
                let y = rng.gen_range(-height_span..height_span) as f32;

                let start = (x, y);
                
                // compute pivot
                let width_span = window_size.width / 4.0;
                let height_span = window_size.height / 3.0+ 50.0;

                let pivot = (rng.gen_range(-width_span..width_span), rng.gen_range(0.0..height_span));

                // compute radius
                let radius = (rng.gen_range(80.0..150.0), 100.0);

                // and start angle
                let angle = (y - pivot.1).atan2(x - pivot.0);

                // and speed
                let speed = BASE_SPEED;

                let formation = Formation {
                    start,
                    radius,
                    pivot,
                    speed,
                    angle,
                };
                
                // store it! (and set new member number)
                self.current_template = Some(formation.clone());
                self.current_members = 1;

                return formation;
            }

        }
    }
}