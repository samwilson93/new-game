use self::formation::{FormationMaker, Formation};
use std::f32::consts::PI;

use bevy::{core::FixedTimestep, ecs::schedule::ShouldRun, prelude::*};
use rand::{thread_rng, Rng};

mod formation;

use crate::{
    components::{Enemy, FromEnemy, Laser, Movable, SpriteSize, Velocity},
    EnemyCount, GameTextures, WindowSize, BASE_SPEED, ENEMY_LASER_SIZE, ENEMY_SIZE,
    SPRITE_SCALE, TIME_STEP, Config,
};

pub struct EnemyPlugin;

impl Plugin for EnemyPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(FormationMaker::default())
            .add_system_set(
                SystemSet::new()
                    .with_run_criteria(FixedTimestep::step(1.0))
                    .with_system(enemy_spawn_system),
            )
            .add_system_set(
                SystemSet::new()
                    .with_run_criteria(enemy_fire_criteria)
                    .with_system(enemy_fire_system),
            )
            .add_system(enemy_movement_system);
    }
}

fn enemy_spawn_system(
    mut commands: Commands,
    game_textures: Res<GameTextures>,
    mut enemy_count: ResMut<EnemyCount>,
    mut formation_maker: ResMut<FormationMaker>,
    window_size: Res<WindowSize>,
    config: Res<Config>,
) {
    if enemy_count.0 < config.enemy_max {
        let formation = formation_maker.make(&window_size, config.formation_members_max);
        let (x, y) = formation.start;

        commands
            .spawn_bundle(SpriteBundle {
                texture: game_textures.enemy.clone(),
                transform: Transform {
                    translation: Vec3::new(x, y, 10.0),
                    scale: Vec3::new(SPRITE_SCALE, SPRITE_SCALE, 1.),
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(formation)
            .insert(Enemy)
            .insert(SpriteSize::from(ENEMY_SIZE));

        enemy_count.0 += 1;
    }
}

fn enemy_fire_criteria() -> ShouldRun {
    if thread_rng().gen_bool(1.0 / 60.0) {
        return ShouldRun::Yes;
    } else {
        return ShouldRun::No;
    }
}

fn enemy_fire_system(
    mut commands: Commands,
    game_textures: Res<GameTextures>,
    enemy_query: Query<&Transform, With<Enemy>>,
) {
    for &transform in enemy_query.iter() {
        let (x, y) = (transform.translation.x, transform.translation.y);
        commands
            .spawn_bundle(SpriteBundle {
                texture: game_textures.enemy_laser.clone(),
                transform: Transform {
                    translation: Vec3::new(x, y - 15.0, 0.0),
                    rotation: Quat::from_rotation_x(PI),
                    scale: Vec3::new(SPRITE_SCALE, SPRITE_SCALE, 1.0),
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Laser)
            .insert(SpriteSize::from(ENEMY_LASER_SIZE))
            .insert(FromEnemy)
            .insert(Movable { auto_despawn: true })
            .insert(Velocity { x: 0.0, y: -1.0 });
    }
}

fn enemy_movement_system(time: Res<Time>, mut query: Query<(&mut Transform, &mut Formation), With<Enemy>>) {
    let now = time.seconds_since_startup() as f32;
    for (mut transform, mut formation) in query.iter_mut() {

        let translation = &mut transform.translation;
        let (x_original, y_original) = (translation.x, translation.y);

        let max_distance = TIME_STEP * formation.speed;

        let direction: f32 = if formation.start.0 < 0.0 { 1.0 } else { -1.0 }; // 1 is clockwise -1 counter clockwise
        let (x_pivot, y_pivot) = formation.pivot;
        let (x_radius, y_radius) = formation.radius;

        let angle = formation.angle 
            + direction * formation.speed * TIME_STEP / (x_radius.min(y_radius) * PI / 2.0);

        let x_distance = x_radius * angle.cos() + x_pivot;
        let y_distance = y_radius * angle.sin() + y_pivot;

        let x_delta = x_original - x_distance;
        let y_delta = y_original - y_distance;

        let distance = (x_delta * x_delta + y_delta * y_delta).sqrt();

        let distance_ratio = if distance != 0.0 {
            max_distance / distance
        } else {
            0.0
        };

        let x = x_original - x_delta * distance_ratio;
        let x = if x_delta > 0.0 {
            x.max(x_distance)
        } else {
            x.min(x_distance)
        };

        let y = y_original - y_delta * distance_ratio;
        let y = if y_delta > 0.0 {
            y.max(y_distance)
        } else {
            y.min(y_distance)
        };

        // Start spin only when we're near the ellipse
        if distance < max_distance * formation.speed / 20.0 {
            formation.angle = angle;
        }

        (translation.x, translation.y) = (x, y);
    }
}
