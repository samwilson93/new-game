use bevy::{core::FixedTimestep, ecs::query, prelude::*};

use crate::{
    components::{FromPlayer, Laser, Movable, Player, SpriteSize, Velocity},
    movable_system, GameTextures, PlayerState, WindowSize, BASE_SPEED, PLAYER_LASER_SIZE,
    PLAYER_SIZE, SPRITE_SCALE, TIME_STEP, Config,
};

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app
            .insert_resource(PlayerState::default())
            .add_system_set(
                SystemSet::new()
                    .with_run_criteria(FixedTimestep::step(0.5))
                    .with_system(player_spawn_system),
            )
            .add_system(player_keyboard_event_system)
            .add_system(player_fire_system);
    }
}

fn player_spawn_system(
    mut commands: Commands,
    mut player_state: ResMut<PlayerState>,
    time: Res<Time>,
    config: Res<Config>,
    game_textures: Res<GameTextures>,
    window_size: Res<WindowSize>,
) {
    let now = time.seconds_since_startup();
    let last_shot = player_state.last_shot;

    if !player_state.on && (last_shot == -1.0 || now > last_shot + config.player_spawn_delay) {
        // Add stuff!
        let bottom = -window_size.height / 2.0;

        commands
            .spawn_bundle(SpriteBundle {
                texture: game_textures.player.clone(),
                transform: Transform {
                    translation: Vec3::new(
                        0.0,
                        bottom + PLAYER_SIZE.1 / 2.0 * SPRITE_SCALE + 5.0,
                        10.0,
                    ),
                    scale: Vec3::new(SPRITE_SCALE, SPRITE_SCALE, 1.0),
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Player)
            .insert(SpriteSize::from(PLAYER_SIZE))
            .insert(Velocity { x: 0.0, y: 0.0 })
            .insert(Movable {
                auto_despawn: false,
            });

        player_state.spawn();
    }
}

fn player_fire_system(
    mut commands: Commands,
    keyboard: Res<Input<KeyCode>>,
    game_textures: Res<GameTextures>,
    query: Query<&Transform, With<Player>>,
) {
    if let Ok(player_transform) = query.get_single() {
        if keyboard.just_pressed(KeyCode::Space) {
            let (x, y) = (
                player_transform.translation.x,
                player_transform.translation.y,
            );
            let offset = PLAYER_SIZE.0 / 2.0 * SPRITE_SCALE - 5.0;

            let mut spawn_laser = |offset: f32| {
                commands
                    .spawn_bundle(SpriteBundle {
                        texture: game_textures.player_laser.clone(),
                        transform: Transform {
                            translation: Vec3::new(x + offset, y + 15.0, 0.0),
                            scale: Vec3::new(SPRITE_SCALE, SPRITE_SCALE, 1.0),
                            ..Default::default()
                        },
                        ..Default::default()
                    })
                    .insert(Laser)
                    .insert(FromPlayer)
                    .insert(SpriteSize::from(PLAYER_LASER_SIZE))
                    .insert(Movable { auto_despawn: true })
                    .insert(Velocity { x: 0.0, y: 1.0 });
            };

            spawn_laser(offset);
            spawn_laser(-offset);
        }
    }
}

fn player_keyboard_event_system(
    keyboard: Res<Input<KeyCode>>,
    mut query: Query<&mut Velocity, With<Player>>,
) {
    if let Ok(mut velocity) = query.get_single_mut() {
        velocity.x = if keyboard.pressed(KeyCode::Left) {
            -1.0
        } else if keyboard.pressed(KeyCode::Right) {
            1.0
        } else {
            0.0
        }
    }
}
