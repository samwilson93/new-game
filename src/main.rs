#![allow(unused)]
mod components;
mod enemy;
mod player;

use std::collections::HashSet;

use bevy::{ecs::query, prelude::*, render::view::window, sprite::collide_aabb::collide};
use components::{
    Enemy, Explosion, ExplosionTimer, ExplosionToSpawn, FromEnemy, FromPlayer, Laser, Movable,
    Player, Score, SpriteSize, Velocity,
};
use enemy::EnemyPlugin;
use figment::{
    providers::{Format, Serialized, Toml},
    Figment,
};
use player::PlayerPlugin;
use serde::{Deserialize, Serialize};

// region: Assets

const PLAYER_SPRITE: &str = "player_a_01.png";
const PLAYER_LASER: &str = "laser_a_01.png";
const PLAYER_SIZE: (f32, f32) = (144.0, 75.0);
const PLAYER_LASER_SIZE: (f32, f32) = (9.0, 54.0);

const ENEMY_SPRITE: &str = "enemy_a_01.png";
const ENEMY_LASER: &str = "laser_b_01.png";
const ENEMY_LASER_SIZE: (f32, f32) = (17.0, 55.0);
const ENEMY_SIZE: (f32, f32) = (144.0, 75.0);

const EXPLOSION_SHEET: &str = "explo_a_sheet.png";

const FONT_LOCATION: &str = "Karla-Regular.ttf";

// endregion: Assets

// region:      -- Game Constants

const SPRITE_SCALE: f32 = 0.5;

const EXPLOSION_LEN: usize = 16;

// endregion:   -- Game Constants

// region: Resources

pub struct WindowSize {
    pub width: f32,
    pub height: f32,
}

struct GameTextures {
    player: Handle<Image>,
    player_laser: Handle<Image>,
    enemy: Handle<Image>,
    enemy_laser: Handle<Image>,
    explosion: Handle<TextureAtlas>,
}

struct EnemyCount(u32);

struct PlayerScore(i128);

struct PlayerState {
    on: bool,       // Spawned/alive
    last_shot: f64, // -1 if not shot
}

impl Default for PlayerState {
    fn default() -> Self {
        Self {
            on: false,
            last_shot: -1.0,
        }
    }
}

impl PlayerState {
    pub fn shot(&mut self, time: f64) {
        self.on = false;
        self.last_shot = time;
    }

    pub fn spawn(&mut self) {
        self.on = true;
        self.last_shot = -1.0;
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct Config {
    enemy_max: u32,
    player_spawn_delay: f64,
    formation_members_max: u32,
}

impl ::std::default::Default for Config {
    fn default() -> Self {
        return Self {
            enemy_max: 2,
            player_spawn_delay: 2.0,
            formation_members_max: 2,
        };
    }
}

// endregion: Resources

// region: Constants

const TIME_STEP: f32 = 1.0 / 60.0;
const BASE_SPEED: f32 = 500.0;

// endregion: Constants

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::rgb(0.04, 0.04, 0.04)))
        .insert_resource(WindowDescriptor {
            title: "Darkest Descent".to_string(),
            width: 600.,
            height: 750.,
            ..Default::default()
        })
        .add_plugins(DefaultPlugins)
        .add_plugin(PlayerPlugin)
        .add_plugin(EnemyPlugin)
        .add_startup_system(setup_system)
        .add_system(movable_system)
        .add_system(player_laser_hit_system)
        .add_system(enemy_laser_hit_player)
        .add_system(explosion_to_spawn_system)
        .add_system(explosion_animation_system)
        .add_system(text_update_system)
        .run();
}

fn setup_system(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    mut windows: ResMut<Windows>,
) {
    // Camera
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());

    // get window size
    let window = windows.get_primary_mut().unwrap();
    let (window_width, window_height) = (window.width(), window.height());

    // Position window
    window.set_position(IVec2::new(1000, 500));

    let window_size = WindowSize {
        width: window_width,
        height: window_height,
    };

    //Setup resources
    commands.insert_resource(window_size);

    // let config: Config = confy::load("space_pruttvaders").unwrap();

    let config: Config = Figment::from(Serialized::defaults(Config::default()))
        .merge(Toml::file("assets/config.toml"))
        .extract()
        .unwrap();

    print!("{:?}", config);
    // print!("{}", toml::to_string(&Config::default()).unwrap());

    // create explosion texture atlas
    let texture_handle = asset_server.load(EXPLOSION_SHEET);
    let texture_atlas = TextureAtlas::from_grid(texture_handle, Vec2::new(64.0, 64.0), 4, 4);
    let explosion = texture_atlases.add(texture_atlas);

    let game_textures = GameTextures {
        player: asset_server.load(PLAYER_SPRITE),
        player_laser: asset_server.load(PLAYER_LASER),
        enemy: asset_server.load(ENEMY_SPRITE),
        enemy_laser: asset_server.load(ENEMY_LASER),
        explosion,
    };

    commands.insert_resource(game_textures);
    commands.insert_resource(EnemyCount(0));
    commands.insert_resource(config);
    commands.insert_resource(PlayerScore(0));

    // Rich text with multiple sections
    commands.spawn_bundle(UiCameraBundle::default());
    commands
        .spawn_bundle(TextBundle {
            style: Style {
                align_self: AlignSelf::FlexEnd,
                ..default()
            },
            // Use `Text` directly
            text: Text {
                // Construct a `Vec` of `TextSection`s
                sections: vec![
                    TextSection {
                        value: "Score: ".to_string(),
                        style: TextStyle {
                            font_size: 16.0,
                            color: Color::WHITE,
                            font: asset_server.load(FONT_LOCATION),
                        },
                    },
                    TextSection {
                        value: "".to_string(),
                        style: TextStyle {
                            font_size: 16.0,
                            color: Color::WHITE,
                            font: asset_server.load(FONT_LOCATION),
                        },
                    },
                ],
                ..default()
            },
            ..default()
        })
        .insert(Score);
}

fn movable_system(
    mut commands: Commands,
    window_size: Res<WindowSize>,
    mut query: Query<(Entity, &Velocity, &mut Transform, &Movable)>,
) {
    for (entity, velocity, mut transform, movable) in query.iter_mut() {
        let translation = &mut transform.translation;
        translation.x += velocity.x * TIME_STEP * BASE_SPEED;
        translation.y += velocity.y * TIME_STEP * BASE_SPEED;

        if movable.auto_despawn {
            // despawn when out of screen
            const MARGIN: f32 = 200.0;
            if translation.y > window_size.height / 2.0 + MARGIN
                || translation.y < -window_size.height / 2.0 - MARGIN
                || translation.x > window_size.width / 2.0 + MARGIN
                || translation.x < -window_size.width / 2.0 - MARGIN
            {
                commands.entity(entity).despawn();
            }
        }
    }
}

fn enemy_laser_hit_player(
    mut commands: Commands,
    mut player_state: ResMut<PlayerState>,
    mut enemy_count: ResMut<EnemyCount>,
    mut player_score: ResMut<PlayerScore>,
    time: Res<Time>,
    laser_query: Query<(Entity, &Transform, &SpriteSize), (With<Laser>, With<FromEnemy>)>,
    player_query: Query<(Entity, &Transform, &SpriteSize), With<Player>>,
) {
    if let Ok((player_entity, player_transform, player_size)) = player_query.get_single() {
        let player_scale = Vec2::from((player_transform.scale.x, player_transform.scale.y));

        for (laser_entity, laser_transform, laser_size) in laser_query.iter() {
            let laser_scale = Vec2::from((laser_transform.scale.x, laser_transform.scale.y));

            let collision = collide(
                laser_transform.translation,
                laser_size.0 * laser_scale,
                player_transform.translation,
                player_size.0 * player_scale,
            );

            if let Some(_) = collision {
                commands.entity(player_entity).despawn();
                player_state.shot(time.seconds_since_startup());

                commands.entity(laser_entity).despawn();

                commands
                    .spawn()
                    .insert(ExplosionToSpawn(player_transform.translation));

                player_score.0 -= 1000;

                break;
            }
        }
    }
}

fn player_laser_hit_system(
    mut commands: Commands,
    mut enemy_count: ResMut<EnemyCount>,
    mut player_score: ResMut<PlayerScore>,
    laser_query: Query<(Entity, &Transform, &SpriteSize), (With<Laser>, With<FromPlayer>)>,
    enemy_query: Query<(Entity, &Transform, &SpriteSize), With<Enemy>>,
) {
    let mut despawned_entities: HashSet<Entity> = HashSet::new();
    for (laser_entity, laser_transform, laser_size) in laser_query.iter() {
        if despawned_entities.contains(&laser_entity) {
            continue;
        }
        let laser_scale = Vec2::new(laser_transform.scale.x, laser_transform.scale.y);

        for (enemy_entity, enemy_transform, enemy_size) in enemy_query.iter() {
            if despawned_entities.contains(&enemy_entity)
                || despawned_entities.contains(&laser_entity)
            {
                continue;
            }
            let enemy_scale = Vec2::new(enemy_transform.scale.x, enemy_transform.scale.y);

            let collision = collide(
                laser_transform.translation,
                laser_size.0 * laser_scale,
                enemy_transform.translation,
                enemy_size.0 * enemy_scale,
            );

            if let Some(_) = collision {
                commands.entity(enemy_entity).despawn();
                despawned_entities.insert(enemy_entity);
                enemy_count.0 -= 1;
                player_score.0 += 100;

                commands.entity(laser_entity).despawn();
                despawned_entities.insert(laser_entity);

                //EXPLOSION
                commands
                    .spawn()
                    .insert(ExplosionToSpawn(enemy_transform.translation.clone()));
            }
        }
    }
}

fn explosion_to_spawn_system(
    mut commands: Commands,
    game_textures: Res<GameTextures>,
    query: Query<(Entity, &ExplosionToSpawn)>,
) {
    for (explosion_spawn_entity, explosion_to_spawn) in query.iter() {
        commands
            .spawn_bundle(SpriteSheetBundle {
                texture_atlas: game_textures.explosion.clone(),
                transform: Transform {
                    translation: explosion_to_spawn.0,
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Explosion)
            .insert(ExplosionTimer::default());

        commands.entity(explosion_spawn_entity).despawn();
    }
}

fn explosion_animation_system(
    mut commands: Commands,
    time: Res<Time>,
    mut query: Query<(Entity, &mut ExplosionTimer, &mut TextureAtlasSprite), With<Explosion>>,
) {
    for (entity, mut timer, mut sprite) in query.iter_mut() {
        timer.0.tick(time.delta());
        if timer.0.finished() {
            sprite.index += 1;
            if sprite.index >= EXPLOSION_LEN {
                commands.entity(entity).despawn();
            }
        }
    }
}

fn text_update_system(player_score: Res<PlayerScore>, mut query: Query<&mut Text, With<Score>>) {
    for mut text in query.iter_mut() {
        println!("Current score {:.2}", player_score.0);
        println!("Object {:?}", text);
        text.sections[1].value = format!("{:.2}", player_score.0);
    }
}
