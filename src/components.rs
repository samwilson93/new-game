use bevy::{prelude::Component, math::{Vec2, Vec3}, core::Timer};

// region: Common

#[derive(Component)]
pub struct Velocity {
    pub x: f32,
    pub y: f32,
}

#[derive(Component)]
pub struct Movable {
    pub auto_despawn: bool,
}

#[derive(Component)]
pub struct Laser;

#[derive(Component)]
pub struct SpriteSize(pub Vec2);

impl From<(f32, f32)> for SpriteSize {
    fn from(val: (f32, f32)) -> Self {
        SpriteSize(Vec2::new(val.0, val.1))
    }
}

// The Score related
#[derive(Component)]
pub struct Score;



// endregion: common

// region: player

#[derive(Component)]
pub struct Player;

#[derive(Component)]
pub struct FromPlayer;

// endregion: player

// region: Enemy

#[derive(Component)]
pub struct Enemy;

#[derive(Component)]
pub struct FromEnemy;

// endregion: Enemy

// region:   -- Explosion components

#[derive(Component)]
pub struct Explosion;

#[derive(Component)]
pub struct ExplosionToSpawn(pub Vec3);

#[derive(Component)]
pub struct ExplosionTimer(pub Timer);

impl Default for ExplosionTimer {
    fn default() -> Self {
        Self(Timer::from_seconds(0.05, true))
    }

}

// endregion:-- Explosion components